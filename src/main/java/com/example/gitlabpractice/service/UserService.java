package com.example.gitlabpractice.service;

import com.example.gitlabpractice.domain.User;
import com.example.gitlabpractice.domain.dto.UserDto;
import com.example.gitlabpractice.domain.dto.UserJoinRequest;
import com.example.gitlabpractice.exception.ErrorCode;
import com.example.gitlabpractice.exception.HospitalReviewAppException;
import com.example.gitlabpractice.repository.UserRepository;
import com.example.gitlabpractice.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String secretKey;

    private long expireTimeMs = 1000 * 60 * 60;


    public UserDto join(UserJoinRequest request) {
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user ->{
                    throw new HospitalReviewAppException(ErrorCode.DUPLICATED_USER_NAME, String.format("UserName:%s", request.getUserName()));
                });



        if (request.getUserName().length() < 6)
            throw new HospitalReviewAppException(ErrorCode.SHORT_USERNAME, request.getUserName());

        if (request.getPassword().length() < 8)
            throw new HospitalReviewAppException(ErrorCode.SHORT_PASSWORD, request.getUserName());




        // 회원가입 .save()
        User savedUser = userRepository.save(request.toEntity(encoder.encode(request.getPassword())));
        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .emailAddress(savedUser.getEmailAddress())
                .role(savedUser.getRole())
                .build();
    }

    public User getUserByUserName(String userName) {
        return userRepository.findByUserName(userName)
                .orElseThrow(()->new HospitalReviewAppException(ErrorCode.NOT_FOUND,""));
    }

}
