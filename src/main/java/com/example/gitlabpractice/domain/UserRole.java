package com.example.gitlabpractice.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserRole {

    NORMAL("normal"),ADMIN("Admin");
    private String role;
}
