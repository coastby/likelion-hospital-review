package com.example.gitlabpractice.controller;

import com.example.gitlabpractice.domain.Response;
import com.example.gitlabpractice.service.UserService;
import com.example.gitlabpractice.domain.dto.UserJoinResponse;
import com.example.gitlabpractice.domain.dto.UserJoinRequest;
import com.example.gitlabpractice.domain.dto.UserDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "회원 가입")
    @PostMapping("/join")
    public Response<UserJoinResponse> join (@RequestBody UserJoinRequest userJoinRequest){
        UserDto userDto = userService.join(userJoinRequest);
        return Response.success(new UserJoinResponse(userDto.getUserName(), userDto.getEmailAddress()));
    }

    @GetMapping
    public ResponseEntity<String> hi (){
        return ResponseEntity.ok("bye");
    }
}
